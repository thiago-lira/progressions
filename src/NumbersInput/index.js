import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

export default function NumbersInput({ value, placeholder, onChange }) {
  // TODO: Validate numbers
  return (
    <section className="numbers-input">
      <div className="input">
        <input
          value={value}
          placeholder={placeholder}
          onChange={onChange}
        />
      </div>
    </section>
  );
}

NumbersInput.propTypes = {
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};
