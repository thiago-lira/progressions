import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

export default function NumbersOutput({ output, outlineColor }) {
  return (
    <section className="numbers-output">
      <p style={{ color: outlineColor }}>{output}</p>
    </section>
  );
}

NumbersOutput.propTypes = {
  output: PropTypes.string.isRequired,
  outlineColor: PropTypes.string,
};

NumbersOutput.defaultProps = {
  outlineColor: 'green',
};
