import React from 'react';
import NumbersInput from './NumbersInput';
import NumbersOutput from './NumbersOutput';
import List from './List';

import './App.scss';

class App extends React.Component {
  static isValidInput(value) {
    return /^(\d|\s)*$/.test(value);
  }

  static isValidSequency(value) {
    const listNumbers = value.trim().split(/\s+/);
    return App.isConstantRatio(listNumbers);
  }

  static isConstantRatio(listNumbers) {
    if (listNumbers.length < 2) return false;

    const [a, b] = listNumbers;
    const ratio = b - a;

    return listNumbers.every((number, index) => {
      if (index > 0) {
        const newRatio = number - listNumbers[index - 1];
        return newRatio === ratio;
      }
      return true;
    });
  }

  constructor(props) {
    super(props);
    this.state = {
      input: '',
      isValidAP: false,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  get midTerm() {
    const { isValidAP } = this.state;
    const { listNums } = this;

    if (isValidAP && listNums.length % 2 !== 0) {
      const first = parseFloat(listNums.shift());
      const last = parseFloat(listNums.pop());

      return (first + last) / 2;
    }
    return null;
  }

  get ratio() {
    const { isValidAP } = this.state;
    const [a, b] = this.listNums;

    if (isValidAP) {
      return b - a;
    }
    return null;
  }

  get listNums() {
    const { input } = this.state;
    return input.trim().split(/\s+/);
  }

  get somatory() {
    const { listNums } = this;
    const total = listNums.length;

    if (total > 1) {
      const first = parseFloat(listNums.shift());
      const last = parseFloat(listNums.pop());

      return (first + last) * (total / 2);
    }
    return null;
  }


  handleChange({ target }) {
    const { value } = target;
    if (App.isValidInput(value)) {
      this.setState({
        input: value,
        isValidAP: App.isValidSequency(value),
      });
    }
  }

  render() {
    const { input, isValidAP } = this.state;
    const items = [{
      field: 'Razão',
      value: this.ratio || '--',
    }, {
      field: 'Somatório',
      value: this.somatory || '--',
    }, {
      field: 'Termo médio',
      value: this.midTerm || '--',
    }];

    return (
      <div className="app">
        <NumbersInput
          placeholder="Sequência numérica separada por espaço"
          value={input}
          onChange={this.handleChange}
        />
        <div className="numbers-output">
          <NumbersOutput
            output={`{${this.listNums.join(', ')}}`}
            outlineColor={isValidAP ? 'green' : 'red'}
          />
        </div>
        <List items={items} />
      </div>
    );
  }
}

export default App;
