import React from 'react';
import PropTypes from 'prop-types';
import ListItem from './ListItem';
import './style.scss';

export default function List({ items }) {
  const createItems = () => items.map(
    ({ field, value }) => (
      <div className="list-item">
        <ListItem key={field} item={({ field, value })} />
      </div>
    ),
  );

  return (
    <section className="list">
      {createItems()}
    </section>
  );
}

List.propTypes = {
  items: PropTypes.arrayOf(Number).isRequired,
};
