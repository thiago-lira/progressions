import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';

export default function ListItem({ item }) {
  return (
    <section className="item">
      <div className="item__field">{ item.field }</div>
      <div className="item__value">{ item.value }</div>
    </section>
  );
}

ListItem.propTypes = {
  item: PropTypes.shape({
    field: PropTypes.string,
    value: PropTypes.string,
  }).isRequired,
};
